require 'rails_helper'

RSpec.describe "Staffs", type: :request do
  let(:staff_administrator) { FactoryGirl.create(:staff, :with_user, role: "Administrator") }
  let(:staff) { FactoryGirl.create(:staff, :with_user, role: "Staff")}

  describe "GET /staffs" do

    context "when signed in user role is 'Administrator'" do
      before do
        post_via_redirect user_session_path, "user[login]" => staff_administrator.user.username, "user[password]" => "1234rewq"
      end

      it "should return http response code to eq '200'" do
        get staffs_path
        expect(response).to have_http_status(200)
      end
    end

    context "when signed in user role is 'Staff'" do
      before do
        post_via_redirect user_session_path, "user[login]" => staff.user.username, "user[password]" => "1234rewq"
      end

      it "should return http response code to eq '404'" do
        get staffs_path
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "GET /staffs/:id" do
    before do
      FactoryGirl.create_list(:staff, 9, :with_user)
    end

    context "when users signed in as staff with 'Administrator' role" do
      before do
        post_via_redirect user_session_path, "user[login]" => staff_administrator.user.username, "user[password]" => "1234rewq"
      end

      it "should return http status code to eq '200'" do
        get staff_path(id: Staff.first)
        expect(response).to have_http_status(200)
      end

      it "should return http status code to eq '404' when staff is not found" do
        allow(Staff).to receive(:find).with("1").and_raise ActiveRecord::RecordNotFound
        get staff_path(id: 1)
        expect(response).to have_http_status(404)
      end
    end

    context "when users signed in as staff with 'Staff' role" do
      before do
        post_via_redirect user_session_path, "user[login]" => staff.user.username, "user[password]" => "1234rewq"
      end

      it "should return http status code to eq '404'" do
        get staff_path(id: Staff.first)
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "GET /staffs/:id/edit" do
    before do
      FactoryGirl.create_list(:staff, 9, :with_user)
    end

    context "when users signed in as staff with 'Administrator' role" do
      before do
        post_via_redirect user_session_path, "user[login]" => staff_administrator.user.username, "user[password]" => "1234rewq"
      end

      it "should return http status code to eq '200'" do
        get edit_staff_path(id: Staff.first)
        expect(response).to have_http_status(200)
      end

      it "should return http status code to eq '404' when staff is not found" do
        allow(Staff).to receive(:find).with("1").and_raise ActiveRecord::RecordNotFound
        get edit_staff_path(id: 1)
        expect(response).to have_http_status(404)
      end
    end

    context "when users signed in as staff with 'Staff' role" do
      before do
        post_via_redirect user_session_path, "user[login]" => staff.user.username, "user[password]" => "1234rewq"
      end

      it "should return http status code to eq '404'" do
        get edit_staff_path(id: Staff.first)
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "GET /staffs/new" do

    context "when signed in user role is 'Administrator'" do
      before do
        post_via_redirect user_session_path, "user[login]" => staff_administrator.user.username, "user[password]" => "1234rewq"
      end

      it "should return http response code to eq '200'" do
        get new_staff_path
        expect(response).to have_http_status(200)
      end
    end

    context "when signed in user role is 'Staff'" do
      before do
        post_via_redirect user_session_path, "user[login]" => staff.user.username, "user[password]" => "1234rewq"
      end

      it "should return http response code to eq '404'" do
        get new_staff_path
        expect(response).to have_http_status(404)
      end
    end
  end


  describe "POST /staffs" do
    context "when signed in user role is 'Administrator'" do
      before do
        post_via_redirect user_session_path, "user[login]" => staff_administrator.user.username, "user[password]" => "1234rewq"
      end

      context "with valid staff data" do
        it "should return http response code to eq '302'" do
          staff = FactoryGirl.build(:staff)
          post staffs_path, staff: staff.attributes
          expect(response).to have_http_status(302)
        end
      end

      context "with invalid staff data" do
        it "should return http response code to eq '200'" do
          staff = FactoryGirl.build(:staff)
          allow_any_instance_of(Staff).to receive(:valid?).and_return(false)
          post staffs_path, staff: staff.attributes
          expect(response).to have_http_status(200)
        end
      end
    end

    context "when signed in user role is 'Staff'" do
      before do
        post_via_redirect user_session_path, "user[login]" => staff.user.username, "user[password]" => "1234rewq"
      end

      it "should return http response code to eq '404'" do
        staff = FactoryGirl.build(:staff)
        post staffs_path, staff: staff.attributes
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "PUT /staff/:id" do
    context "when signed in staff role is 'Administrator'" do
      before do
        post_via_redirect user_session_path, "user[login]" => staff_administrator.user.username, "user[password]" => "1234rewq"
      end

      context "when updated staff with valid data" do
        it "should given http response code to eq '302'" do
          staff1 = FactoryGirl.create(:staff)
          staff2 = FactoryGirl.build(:staff)
          put staff_path(id: staff1.id), staff: staff2.attributes
          expect(response).to have_http_status(302)
        end
      end

      context "when updated staff with invalid data" do
        it "should given http response code to eq '200'" do
          staff1 = FactoryGirl.create(:staff)
          staff2 = FactoryGirl.build(:staff)
          allow_any_instance_of(Staff).to receive(:valid?).and_return(false)
          put staff_path(id: staff1.id), staff: staff2.attributes
          expect(response).to have_http_status(200)
        end
      end

      context "when updated staff that doesn't exist" do
        it "should given http response code to eq '404'" do
          allow(Staff).to receive(:find).with("1").and_raise ActiveRecord::RecordNotFound
          staff2 = FactoryGirl.build(:staff)
          put staff_path(id: 1), staff: staff2.attributes
          expect(response).to have_http_status(404)
        end
      end
    end

    context "when signed in staff role is 'Staff'" do
      before do
        post_via_redirect user_session_path, "user[login]" => staff.user.username, "user[password]" => "1234rewq"
      end

      it "should given http response code to eq '404'" do
        allow(Staff).to receive(:find).with("1").and_raise ActiveRecord::RecordNotFound
        staff2 = FactoryGirl.build(:staff)
        put staff_path(id: 1), staff: staff2.attributes
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "DELETE /staffs/:id" do
    context "when signed in staff role is 'Administrator'" do
      before do
        post_via_redirect user_session_path, "user[login]" => staff_administrator.user.username, "user[password]" => "1234rewq"
      end

      context "when staffs is available" do
        it "should given http response code to eq '302'" do
          staff = FactoryGirl.create(:staff)
          delete staff_path(id: staff.id)
          expect(response).to have_http_status(302)
        end
      end

      context "when staff is not available" do
        it "should giveh http response code to eq '404'" do
          allow(Staff).to receive(:find).with("1").and_raise ActiveRecord::RecordNotFound
          delete staff_path(id: 1)
          expect(response).to have_http_status(404)
        end
      end
    end

    context "when sigened in staff role is 'Staff'" do
      before do
        post_via_redirect user_session_path, "user[login]" => staff.user.username, "user[password]" => "1234rewq"
      end

      it "should giveh http response code to eq '404'" do
        staff = FactoryGirl.create(:staff)
        delete staff_path(id: staff.id)
        expect(response).to have_http_status(404)
      end
    end
  end
end
