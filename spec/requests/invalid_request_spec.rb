require 'rails_helper'

RSpec.describe "InvalidRequest" do

  describe "invalid request using get method" do
    it "should render error page 404" do
      get "/invlid_request"
      expect(response).to have_http_status 404
    end
  end

  describe "invalid request using post method" do
    it "should render error page 404" do
      post "/invlid_request"
      expect(response).to have_http_status 404
    end
  end

  describe "invalid request using put method" do
    it "should render error page 404" do
      post "/invlid_request"
      expect(response).to have_http_status 404
    end
  end

  describe "invalid request using delete method" do
    it "should render error page 404" do
      delete "/invlid_request"
      expect(response).to have_http_status 404
    end
  end
end
