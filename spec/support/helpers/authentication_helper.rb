module AuthenticationHelper
  def sign_in user, password = nil
    password = password || "1234rewq"
    visit "/"
    within("#new_user") do
      fill_in "user[login]", with: user.username
      fill_in "user[password]", with: password
    end
    click_button "Log in"
  end
end
