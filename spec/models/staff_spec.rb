require 'rails_helper'

RSpec.describe Staff, type: :model do
  let(:staff) { FactoryGirl.build(:staff, :with_user) }
  subject { staff }

  describe "Validations" do
    it { is_expected.to validate_presence_of :first_name }
    it { is_expected.to validate_presence_of :sex }
    it { is_expected.to validate_presence_of :role }
    it { is_expected.to validate_inclusion_of(:sex).in_array(["Female", "Male"])}
    it { is_expected.to validate_inclusion_of(:role).in_array(["Administrator", "Staff"])}
  end

  describe "#user" do
    it "should be kind of User object" do
      staff.save
      expect(staff.user).to be_a User
    end

    it "should validate user object when user params is assign" do
      staff = Staff.new(user: User.new )
      expect(staff).not_to be_valid
      expect(staff.errors).to include :user
    end
  end

  describe "#display_name" do

    it "should concate fornt_title, first_name, last_name, and back_title if everything is complete" do
      staff.front_title = "DR"
      staff.back_title = "M. Si"
      expect(staff.display_name).to eq "#{staff.front_title}. #{staff.first_name} #{staff.last_name}, #{staff.back_title}"
    end

    context "when no front title and back title" do
      before do
        staff.front_title = ""
        staff.back_title = ""
      end

      it "should concate first_name with last_name if the last_name is available" do
        expect(staff.display_name).to eq "#{staff.first_name} #{staff.last_name}"
      end

      it "should return first_name if last_name is not available" do
        staff.last_name = ""
        expect(staff.display_name).to eq "#{staff.first_name}"
      end
    end

    context "when no front title but back title is available" do
      before do
        staff.front_title = ""
        staff.back_title = "S. Kom"
      end

      it "should concate first_name, last_name and back_title if the last_name is available" do
        expect(staff.display_name).to eq "#{staff.first_name} #{staff.last_name}, #{staff.back_title}"
      end

      it "should concate first_name with back_title if the last_name is not available" do
        staff.last_name = ""
        expect(staff.display_name).to eq "#{staff.first_name}, #{staff.back_title}"
      end
    end

    context "when front_title is availabe but not back_title" do
      before do
        staff.front_title = "Dr"
        staff.back_title = ""
      end

      it "should concate front_title, first_name, and last_name if the last_name is available" do
        expect(staff.display_name).to eq "#{staff.front_title}. #{staff.first_name} #{staff.last_name}"
      end

      it "should concate first_title with first_name if the last_name is not available" do
        staff.last_name = ""
        expect(staff.display_name).to eq "#{staff.front_title}. #{staff.first_name}"
      end
    end

  end


  describe ".by_full_name" do
    before do
      FactoryGirl.create(:staff, first_name: "Fauzan", last_name: "Qadri")
      FactoryGirl.create(:staff, first_name: "Sherlock", last_name: "Holmes")
    end

    it "should return array that contain full name given by args when available" do
      staffs1 = Staff.by_full_name("Fauzan")
      staffs2 = Staff.by_full_name("Holmes")
      expect(staffs1.size).to eq 1
      expect(staffs1.first.display_name).to match /Fauzan/
      expect(staffs2.size).to eq 1
      expect(staffs2.last.display_name).to match /Holmes/
    end

    it "should return an empty array if not found" do
      staffs = Staff.by_full_name("Not Available")
      expect(staffs.size).to eq 0
    end
  end

  describe ".by_gender" do
    before do
      FactoryGirl.create_list(:staff, 100, sex: "Male")
      FactoryGirl.create_list(:staff, 100, sex: "Female")
    end

    it "should ordered object based on argument" do
      staffs1 = Staff.by_gender("male")
      staffs2 = Staff.by_gender("female")
      expect(staffs1.pluck(:sex)).not_to include "Female"
      expect(staffs2.pluck(:sex)).not_to include "Male"
    end

    it "should given an empty array if gander not found" do
      staffs = Staff.by_gender("uni sex")
      expect(staffs.size).to eq 0
    end
  end
end
