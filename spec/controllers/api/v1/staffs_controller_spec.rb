require 'rails_helper'
RSpec.describe Api::V1::StaffsController, type: :controller do


  describe "GET #index" do

    before(:each) do
      FactoryGirl.create_list(:staff, 100, :with_user)
    end

    context "without params" do
      before(:each) do
        get :index, {format: :json}
      end

      it "should given http response code '200'" do
        expect(response).to be_ok
      end

      it "assigns Api::V1::Staffs::Index as @presenter" do
        expect(assigns(:presenter)).to be_a Api::V1::Staffs::Index
      end

      it "should response json format" do
        expect(response.headers).to include("Content-Type")
        expect(response.headers["Content-Type"]).to match /application\/json/
      end

    end

    context "with params" do
      before(:each) do
        get :index, {page: 2, per_page: 25, by_full_name: "Name", format: :json}
      end

      it "should given http response code '200'" do
        expect(response).to be_ok
      end

      it "assigns Api::V1::Staffs::Index as @presenter" do
        expect(assigns(:presenter)).to be_a Api::V1::Staffs::Index
      end

      it "should response json format" do
        expect(response.headers).to include("Content-Type")
        expect(response.headers["Content-Type"]).to match /application\/json/
      end

    end

  end

  describe "GET #show" do
    #let(:staff) {FactoryGirl.create(:staff, :with_user)}
    context "when staff available" do
      it "assigns the requested staff as @staff" do
        staff = FactoryGirl.create(:staff, :with_user)
        get :show, {id: staff.to_param, format: :json}
        expect(assigns(:staff)).to eq staff
      end
    end

    context "when staff unavailable" do
      it "respons code should eq '404'" do
        #allow(Staff).to receive(:find).with(staff.id).and_raise ActiveRecord::RecordNotFound
        get :show, {id: 2, format: :json}
        expect(response.code).to eq "404"
      end
    end
  end
end
