require 'rails_helper'
RSpec.describe Api::V1::Staffs::Index, type: :presenter do

  describe "#staffs" do
    before do
      FactoryGirl.create_list(:staff, 100)
    end

    context "when initialize without given any argument" do
      let(:index_presenter) {Api::V1::Staffs::Index.new}

      it "should given 10 list of staffs" do
        staffs = Staff.first(10)
        expect(index_presenter.staffs).to eq staffs
      end
    end

    context "when initialize with only given argument page" do
      let(:index_presenter) {Api::V1::Staffs::Index.new(page: 2)}

      it "should given requested staffs on that page" do
        staffs = Staff.page(2).per(10)
        expect(index_presenter.staffs).to eq staffs
      end
    end

    context "when initailize with only given argument per page" do
      let(:index_presenter) {Api::V1::Staffs::Index.new(per_page: 25)}

      it "should given requested per page staffs" do
        staffs = Staff.first(25)
        expect(index_presenter.staffs).to eq staffs
      end
    end

    context "when initialize with args by full name" do
      let(:index_presenter) {Api::V1::Staffs::Index.new(by_full_name: "Fauzan")}

      before do
        FactoryGirl.create(:staff, first_name: "Fauzan", last_name: "Qadri")
      end

      it "should search staff that have full name given the args" do
        staffs = Staff.by_full_name("Fauzan")
        expect(index_presenter.staffs.first.id).to eq staffs.first.id
      end

    end

    context "when initialize with given argument by_full_name, page and per_page" do
      let(:page) {2}
      let(:per_page) {25}
      let(:index_presenter) {Api::V1::Staffs::Index.new(page: page, per_page: per_page, by_full_name: "Fauzan")}

      before do
        FactoryGirl.create(:staff, first_name: "Fauzan", last_name: "Qadri")
      end

      it "should given requested staffs on that page" do
        staffs = Staff.by_full_name("Fauzan").page(page).per(per_page)
        expect(index_presenter.staffs).to eq staffs
      end

    end
  end

  describe "#first_page?" do
    it "should return whether its first page or not" do
      FactoryGirl.create_list(:staff, 100)
      index_presenter1 = Api::V1::Staffs::Index.new(page: 1, per_page: 25)
      index_presenter2 = Api::V1::Staffs::Index.new(page: 4, per_page: 25)
      expect(index_presenter1.first_page?).to eq true
      expect(index_presenter2.first_page?).to eq false
    end
  end

  describe "#last_page?" do
    it "should return whether its last page or not" do
      FactoryGirl.create_list(:staff, 100)
      index_presenter1 = Api::V1::Staffs::Index.new(page: 1, per_page: 25)
      index_presenter2 = Api::V1::Staffs::Index.new(page: 4, per_page: 25)
      expect(index_presenter1.last_page?).to eq false
      expect(index_presenter2.last_page?).to eq true
    end
  end

  describe "#current_page" do
    it "should return the current page of the staff list" do
      FactoryGirl.create_list(:staff, 100)
      index_presenter1 = Api::V1::Staffs::Index.new(page: 1, per_page: 25)
      index_presenter2 = Api::V1::Staffs::Index.new(page: 4, per_page: 25)
      expect(index_presenter1.current_page).to eq 1
      expect(index_presenter2.current_page).to eq 4
    end
  end

  describe "#total_page" do
    it "should return the total page of the staff list" do
      FactoryGirl.create_list(:staff, 100)
      index_presenter1 = Api::V1::Staffs::Index.new(page: 1, per_page: 25)
      index_presenter2 = Api::V1::Staffs::Index.new(page: 1, per_page: 100)
      expect(index_presenter1.total_pages).to eq 4
      expect(index_presenter2.total_pages).to eq 1
    end
  end
end
