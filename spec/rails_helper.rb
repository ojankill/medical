ENV['RAILS_ENV'] ||= 'test'
require 'spec_helper'
require File.expand_path('../../config/environment', __FILE__)
require 'rspec/rails'
require 'capybara/rspec'
require 'capybara/poltergeist'
require 'database_cleaner'
require 'shoulda/matchers'
require 'simplecov'
SimpleCov.start 'rails'

ActiveRecord::Migration.maintain_test_schema!
#Capybara.javascript_driver = :poltergeist
Capybara.javascript_driver = :webkit
Capybara.ignore_hidden_elements = true
Dir.glob("spec/features/steps/*steps.rb") { |f| load f, true }
Dir[File.dirname(__FILE__) + "/support/**/*.rb"].each {|f| require f }

RSpec.configure do |config|
  config.use_transactional_fixtures = false

  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do
    DatabaseCleaner.strategy = :transaction
  end

  config.before(:each, :js => true) do
    DatabaseCleaner.strategy = :truncation
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end
  #config.before(:all) do
    #DatabaseCleaner.strategy = :transaction
    #DatabaseCleaner.clean_with(:truncation)
  #end

  #config.around(:each, type: [:controller, :model, :view, :helper, :integration, :job, :mailer, :observer, :presenter]) do |example|
    #DatabaseCleaner.strategy = :transaction
    #DatabaseCleaner.clean_with(:truncation)
    #DatabaseCleaner.cleaning do
      #example.run
    #end
  #end

  #config.after(:each, type: [:controller, :model, :view, :helper, :integration, :job, :mailer, :observer, :presenter]) do
    #DatabaseCleaner.clean
  #end

  #config.around(:each, type: :feature, javascript: true) do |example|
    #DatabaseCleaner.strategy = :truncation
    #example.run
    #DatabaseCleaner.strategy = :transaction
  #end

  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.infer_spec_type_from_file_location!
  config.include Devise::TestHelpers, :type => :controller
  config.include AuthenticationHelper, :type => :feature
  config.include ControllerMacros, :type => :controller
end
