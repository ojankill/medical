FactoryGirl.define do
  factory :staff do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    address { "#{Faker::Address.street_address}, #{Faker::Address.city}" }
    phone { Faker::PhoneNumber.phone_number }
    sequence :sex do |n|
      ["Male", "Female"][(n % 2)]
    end
    born { 24.years.ago.to_date }
    back_title "S, Kom"
    sequence :role do |n|
      ["Administrator", "Staff"][(n % 2)]
    end

    trait :with_user do
      association :user
    end

  end

end
