FactoryGirl.define do
  factory :user do
    sequence :email do |n|
      "person#{n}@example.com"
    end

    sequence :username do |n|
      "person#{n}"
    end
    password { "1234rewq" }
    password_confirmation { "1234rewq" }

    trait :with_staff do
      association :userable, factory: :staff
    end
  end

end
