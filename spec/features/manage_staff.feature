@javascript
@manage_staff
Feature: Manage Staff
  Background:
    Given a couple of application user and staff list

  Scenario: Access staff management page without login
    When i visit the staff management page
    Then i should see 404 error page

  Scenario: Access staff management page with administrator role user
    When i sign in as administrator staff and i access staff management page
    Then i should see list of staff
    And i should see add new staff link
    Then i should see delete staff link
    And i should see update staff link
    Then i should see details staff link

  Scenario: As an administrator staff i want to create new staff
    When i sign in as administrator staff and i access staff management page
    Then i click link new staff
    And i should see form for create new staff
    When i fill form with invalid data
    Then i click save button and staff should not be saved
    Then i should see form for create new staff
    When i fill form with valid data
    Then i click save button and staff should be saved
    Then i should not see form for create new staff

  Scenario: As an administrator staff i want to update staff data
    When i sign in as administrator staff and i access staff management page
    Then i click link edit in one of listing staff
    And i should see form for edit staff
    When i fill edit staff form with invalid data
    Then i click save button and staff should not updated
    Then i should see form for edit staff
    When i fill edit staff form with valid data
    Then i click save button and staff should updated
    #Then i should not see form edit staff
