@javascript
@login
Feature: Login
  Background:
    Given there is a user and not loged in
  Scenario: Access application without login
    When i visit the root url of the application
    Then i should see the login form
  Scenario: Login with invalid username and password
    When i visit the root url of the application
    Then i entered invalid username and password
    And i should see the login form
  Scenario: Login with valid username and password
    When i visit the root url of the application
    Then i entered valid username and password
    And i should not see the login form
    Then i should see message Signed in successfully
