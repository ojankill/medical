module LoginSteps
  step "there is a user and not loged in" do
    @user = FactoryGirl.create(:user)
  end

  step "i visit the root url of the application" do
    visit root_url
  end

  step "i should see the login form" do
    expect(page).to have_field "user[login]"
    expect(page).to have_field "user[password]"
    expect(page).to have_button "Log in"
  end

  step "i entered invalid username and password" do
    within("#new_user") do
      fill_in "user[login]", with: "invalid"
      fill_in "user[password]", with: "invalid"
    end
    click_button "Log in"
  end

  step "i entered valid username and password" do
    within("#new_user") do
      fill_in "user[login]", with: @user.username
      fill_in "user[password]", with: "1234rewq"
    end
    click_button "Log in"
  end

  step "i should not see the login form" do
    expect(page).not_to have_field "user[login]"
    expect(page).not_to have_field "user[password]"
    expect(page).not_to have_button "Log in"
  end

  step "i should see message Signed in successfully" do
    expect(page).to have_content "Signed in successfully"
  end
end

steps_for :login do
  include LoginSteps
end
