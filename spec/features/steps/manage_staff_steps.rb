module ManageStaffSteps

  step "a couple of application user and staff list" do
    @staff_administrator = FactoryGirl.create(:staff, :with_user, role: "Administrator")
    @staff = FactoryGirl.create(:staff, :with_user, role: "Staff")
    FactoryGirl.create_list(:staff, 100)
  end

  step "i visit the staff management page" do
    visit "/staffs"
  end

  step "i should see 404 error page" do
    expect(page).to have_content "404"
  end

  step "i sign in as administrator staff and i access staff management page" do
    sign_in @staff_administrator.user
    visit "/staffs"
  end

  step "i should see list of staff" do
    staffs = Staff.first(10)
    expect(page).to have_content "Listing staffs"
    expect(page).to have_content "Full name"
    expect(page).to have_content "Address"
    expect(page).to have_content "Phone"
    expect(page).to have_content "Sex"
    expect(page).to have_content "Role"
    expect(page).to have_content "Action"
    staffs.each do |staff|
      expect(page).to have_content staff.display_name
      expect(page).to have_content staff.address
      expect(page).to have_content staff.phone
      expect(page).to have_content staff.sex
      expect(page).to have_content staff.role
    end
  end

  step "i should see add new staff link" do
    expect(page).to have_link "New Staff"
  end

  step "i should see delete staff link" do
    expect(page).to have_link "Delete", count: 10
  end

  step "i should see update staff link" do
    expect(page).to have_link "Edit", count: 10
  end

  step "i should see details staff link" do
    expect(page).to have_link "Detail", count: 10
  end

  step "i click link new staff" do
    click_link "New Staff"
  end

  step "i should see form for create new staff" do
    expect(page).to have_field "First name"
    expect(page).to have_field "Last name"
    expect(page).to have_field "Address"
    expect(page).to have_field "Phone"
    expect(page).to have_field "Sex"
    #expect(page).to have_field "Born"
    expect(page).to have_field "Front title"
    expect(page).to have_field "Back title"
    expect(page).to have_field "Role"
  end

  step "i fill form with invalid data" do
    within("#new_staff") do
      fill_in "staff[first_name]", with: ""
      fill_in "staff[last_name]", with: ""
      fill_in "staff[address]", with: ""
      fill_in "staff[phone]", with: ""
      fill_in "staff[sex]", with: ""
      find_by_id("staff_born_1i").find("option[value='#{Time.now.year}']").click
      find_by_id("staff_born_2i").find("option[value='#{Time.now.month}']").click
      find_by_id("staff_born_3i").find("option[value='#{Time.now.day}']").click
      fill_in "staff[front_title]", with: ""
      fill_in "staff[back_title]", with: ""
      fill_in "staff[role]", with: ""
    end
  end

  step "i click save button and staff should not be saved" do
    expect{ click_button("Save") }.to change(Staff, :count).by(0)
  end

  step "i fill form with valid data" do
    staff = FactoryGirl.build(:staff)
    within("#new_staff") do
      fill_in "staff[first_name]", with: staff.first_name
      fill_in "staff[last_name]", with: staff.last_name
      fill_in "staff[address]", with: staff.address
      fill_in "staff[phone]", with: staff.phone
      fill_in "staff[sex]", with: staff.sex
      find_by_id("staff_born_1i").find("option[value='#{Time.now.year}']").click
      find_by_id("staff_born_2i").find("option[value='#{Time.now.month}']").click
      find_by_id("staff_born_3i").find("option[value='#{Time.now.day}']").click
      fill_in "staff[front_title]", with: staff.front_title
      fill_in "staff[back_title]", with: staff.back_title
      fill_in "staff[role]", with: staff.role
    end
  end

  step "i click save button and staff should be saved" do
    expect{ click_button("Save") }.to change(Staff, :count).by(1)
  end

  step "i should not see form for create new staff" do
    expect(page).not_to have_field "First name"
    expect(page).not_to have_field "Last name"
    expect(page).not_to have_field "Address"
    expect(page).not_to have_field "Phone"
    expect(page).not_to have_field "Sex"
    #expect(page)not_.to have_field "Born"
    expect(page).not_to have_field "Front title"
    expect(page).not_to have_field "Back title"
    expect(page).not_to have_field "Role"
  end

  step "i click link edit in one of listing staff" do
    all(:link, "Edit").last.click
  end

  step "i should see form for edit staff" do
    step "i should see form for create new staff"
  end

  step "i fill edit staff form with invalid data" do
    within(".edit_staff") do
      fill_in "staff[first_name]", with: ""
      fill_in "staff[last_name]", with: ""
      fill_in "staff[address]", with: ""
      fill_in "staff[phone]", with: ""
      fill_in "staff[sex]", with: ""
      fill_in "staff[front_title]", with: ""
      fill_in "staff[back_title]", with: ""
      fill_in "staff[role]", with: ""
    end
  end

  step "i click save button and staff should not updated" do
    step "i should see form for create new staff"
  end

  step "i fill edit staff form with valid data" do
    staff = FactoryGirl.build(:staff)
    within(".edit_staff") do
      fill_in "staff[first_name]", with: staff.first_name
      fill_in "staff[last_name]", with: staff.last_name
      fill_in "staff[address]", with: staff.address
      fill_in "staff[phone]", with: staff.phone
      fill_in "staff[sex]", with: staff.sex
      fill_in "staff[front_title]", with: staff.front_title
      fill_in "staff[back_title]", with: staff.back_title
      fill_in "staff[role]", with: staff.role
    end
  end

  step "i click save button and staff should updated" do
    click_button "Save"
    staff = Staff.first(10).last
    expect(page).to have_content staff.display_name
  end

end

steps_for :manage_staff do
  include ManageStaffSteps
end
