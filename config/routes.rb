Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do
      resources :staffs
    end
  end

  devise_for :users, skip: [:registration, :sessions, :password]
  devise_scope :user do
    authenticated :user do
      root to: "statics#landing", as: "authenticated_root"
    end


    authenticated :user, lambda {|user| user.userable_type == "Staff" && user.userable.role == "Administrator"} do
      resources :staffs
    end

    unauthenticated do
      root to: "devise/sessions#new"
      post "/" => "devise/sessions#create", as: :user_session
    end
    delete "sign_out" => "devise/sessions#destroy", as: :destroy_user_session
  end

  get "*unmatched_routes", to: "application#raise_not_found!", via: :all
  post "*unmatched_routes", to: "application#raise_not_found!", via: :all
  put "*unmatched_routes", to: "application#raise_not_found!", via: :all
  delete "*unmatched_routes", to: "application#raise_not_found!", via: :all
end
