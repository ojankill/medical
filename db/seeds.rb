# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
admin_attributes = { first_name: "Administrator", sex: "Male", born: 24.years.ago.to_date, role: "Administrator"}
user_attributes = { username: "administrator", email: "example@mail.com" }
staff = Staff.find_or_initialize_by(admin_attributes).tap do |staff|
  staff.save!
end

User.find_or_initialize_by(user_attributes).tap do |user|
  user.userable = staff
  user.password = "1234rewq"
  user.password_confirmation = "1234rewq"
  user.save!
end
