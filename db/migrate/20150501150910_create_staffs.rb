class CreateStaffs < ActiveRecord::Migration
  def change
    create_table :staffs do |t|
      t.string :first_name
      t.string :last_name
      t.text :address
      t.string :phone
      t.string :sex
      t.date :born
      t.string :front_title
      t.string :back_title
      t.string :role

      t.timestamps null: false
    end
  end
end
