class Api::V1::Staffs::Index
  attr_reader :staffs

  def initialize args = {}
    args = ActiveSupport::HashWithIndifferentAccess.new(args)
    @page = args[:page] || 1
    @per_page = args[:per_page] || 10
    @by_full_name = args[:by_full_name]
  end

  def staffs
    @staffs ||= fetch_staffs
  end


  def total_pages
    @total_pages ||= staffs.total_pages
  end

  def first_page?
    @first_page ||= staffs.first_page?
  end


  def last_page?
    @last_page ||= staffs.last_page?
  end

  def current_page
    @current_page ||= staffs.current_page
  end


  private

    def fetch_staffs
      if @by_full_name.present?
        return search_staff_by_full_name(@by_full_name).page(@page).per(@per_page)
      else
        return Staff.includes(:user).page(@page).per(@per_page)
      end
    end

    def search_staff_by_full_name full_name
      Staff.includes(:user).by_full_name(full_name)
    end
end
