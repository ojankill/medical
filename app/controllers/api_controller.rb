class ApiController < ActionController::Base
  protect_from_forgery with: :null_session
  rescue_from ActiveRecord::RecordNotFound, :with => :render_not_found

  def render_not_found(e)
    respond_to do |format|
      format.html { render template: "errors/404", status: 404}
      format.json { render json: {messages: "The resource you're looking for is not available"}, status: 404}
    end
  end
end
