class Api::V1::StaffsController < ApiController
  respond_to :json

  def index
    @presenter = Api::V1::Staffs::Index.new(params)
  end

  def show
    @staff = Staff.includes(:user).find(params[:id])
  end

end
