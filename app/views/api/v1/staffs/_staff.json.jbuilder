json.id staff.id
json.full_name staff.display_name
json.address staff.address
json.phone staff.phone
json.sex staff.sex
json.born staff.born
json.role staff.role
json.user do
  json.email staff.user.try(:email)
  json.username staff.user.try(:username)
end
