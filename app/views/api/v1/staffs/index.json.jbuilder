json.total_pages @presenter.total_pages
json.current_page @presenter.current_page
json.first_page @presenter.first_page?
json.last_page @presenter.last_page?
json.staffs @presenter.staffs do |staff|
  json.partial! "staff", staff: staff
end
