json.array!(@staffs) do |staff|
  json.extract! staff, :id, :first_name, :last_name, :address, :phone, :sex, :born, :front_title, :back_title, :role
  json.url staff_url(staff, format: :json)
end
