class Staff < ActiveRecord::Base
  has_one :user, as: :userable
  accepts_nested_attributes_for :user

  validates :first_name, presence: true
  validates :sex, presence: true, inclusion: { in: SEX}
  validates :role, presence: true, inclusion: { in: ROLE}
  validates_associated :user

  scope :by_full_name, ->(contex) { where{(first_name =~ "%#{contex}") | (last_name =~ "%#{contex}")}}
  scope :by_gender, ->(gender) { where{sex == gender.humanize}}

  def display_name
    return "#{self.front_title}. #{full_name}" if self.front_title.present? && !self.back_title.present?
    return "#{full_name}, #{self.back_title}" if !self.front_title.present? && self.back_title.present?
    return "#{self.front_title}. #{full_name}, #{self.back_title}" if self.front_title.present? && self.back_title.present?
    return full_name
  end


  private

    def full_name
      return "#{self.first_name} #{self.last_name}" if self.last_name.present?
      return self.first_name
    end

end
